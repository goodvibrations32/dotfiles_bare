module Colors.ZenburnDark where

import XMonad

colorScheme = "zenburn-dark"

colorBack = "#171717"
colorFore = "#F0DFAF"

color01 = "#989890"
color02 = "#aa4450"
color03 = "#719611"
color04 = "#d79921"
color05 = "#87ceeb"
color06 = "#528b8b"
color07 = "#8787AF"
color08 = "#a89984"
color09 = "#928374"
color10 = "#ff9800"
color11 = "#7CB8BB"
color12 = "#fabd2f"
color13 = "#6CA0A3"
color14 = "#ECB3B3"
color15 = "#719611"
color16 = "#ebdbb2"

colorTrayer :: String
colorTrayer = "--tint 0x171717"
