module Generics.Defaults where
-- import Graphics.X11 (KeyMask)
import XMonad
import Colors.ZenburnDark
import XMonad.Util.NamedScratchpad
import qualified XMonad.StackSet as W

myFont :: String
myFont = "xft:Hasklug Nerd Font:regular:size=10:antialias=true:hinting=true"
myModMask :: XMonad.KeyMask
myModMask = XMonad.mod4Mask        -- Sets modkey to super/windows key

myTerminal :: String
myTerminal = "alacritty"    -- Sets default terminal

scratchTerminal :: String
scratchTerminal = "wezterm start --class Scratchpad-wez"   -- Sets default terminal

scratchFiles :: String
scratchFiles = "wezterm start --class Ranger -e ranger"   -- Sets default terminal

scratchClock :: String
scratchClock = myTerminal ++ " --class Clocker -e tty-clock -scbx -f '%a, %d %b %Y %T %z'"

myBrowser :: String
myBrowser = "zen-browser"
-- myBrowser = "firefox-developer-edition & xdotool search --sync --onlyvisible --class 'firefox' windowactivate key F11"  -- Sets qutebrowser as browser

myEmacs :: String
-- myEmacs = "emacs"
myEmacs = "emacsclient -c -a '' "  -- Makes emacs keybindings easier to type

myBorderWidth :: XMonad.Dimension
myBorderWidth = 2           -- Sets border width for windows

myNormColor :: String       -- Border color of normal windows
myNormColor   = colorBack   -- This variable is imported from Colors.THEME

myFocusColor :: String      -- Border color of focused windows
myFocusColor  = color07     -- This variable is imported from Colors.THEME

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "calculator" spawnCalc findCalc manageCalc
                , NS "browser" spawnbrowser findbrowser managebrowser
                , NS "files" spawnTheman findTheman manageTheman
                , NS "clock" spawnClock findClock manageClock
                ]
  where
    spawnClock  = scratchClock
    findClock = className =? "Clocker"
    manageClock = customFloating $ W.RationalRect l t w h
               where
                 h = 0.15
                 w = 0.9
                 t = 0.7
                 l = 0.20

    spawnTheman  = scratchFiles
    findTheman = className =? "Ranger"
    manageTheman = customFloating $ W.RationalRect l t w h
               where
                 h = 0.7
                 w = 0.6
                 t = 0.25
                 l = 0.20

    spawnTerm  = scratchTerminal
    findTerm   = className =? "Scratchpad-wez"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.25
                 w = 0.6
                 t = 0.7
                 l = 0.20

    spawnCalc  = "qalculate-gtk"
    findCalc   = className =? "Qalculate-gtk"
    manageCalc = customFloating $ W.RationalRect l t w h
               where
                 h = 0.6
                 w = 0.2
                 t = 0.95 -h
                 l = 0.85 -w

    spawnbrowser  = "firefox"
    findbrowser   = className =? "firefox"
    managebrowser = customFloating $ W.RationalRect l t w h
               where
                 h = 0.8
                 w = 0.2
                 t = 0.15
                 l = 0.80
