module Keys.MyKeys where

import XMonad
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation

import qualified XMonad.StackSet as W
import qualified XMonad.Layout.MultiToggle as MT
import qualified XMonad.Layout.ToggleLayouts as T

import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.WithAll (killAll, sinkAll)
import XMonad.Actions.DynamicWorkspaces (withWorkspace, withNthWorkspace, selectWorkspace, removeWorkspace)
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.CycleWS (nextScreen, prevScreen, WSType (..))
import XMonad.Actions.Promote

import Data.Sequence
import XMonad.Layout.MultiToggle.Instances
import XMonad.Hooks.ManageDocks
import XMonad.Layout.ResizableTile (MirrorResize(..))
import XMonad.Layout.Spacing
import XMonad.Layout.LimitWindows
import XMonad.Util.NamedScratchpad
import XMonad.Actions.WindowGo
import Data.Maybe (isJust)

import Generics.MyWsp
import Generics.Defaults
import XMonad.Layout.TallMastersCombo (FocusedNextLayout(FocusedNextLayout))

myEmKeys :: [(String, X ())]
myEmKeys =
  [("M1-S-q", spawn "archlinux-logout")
  , ("M1-S-r", spawn "xmonad --restart")
  , ("M1-c", kill1)
  , ("M-S-a", killAll)
  , ("S-<Return>", spawn "rofi -m -1 -show drun -show-icons")
  , ("M1-w <Space>", spawn "rofi -m -1 -show window -show-icons")
  , ("M1-w <Return>", spawn "rofi -m -1 -show run -show-icons")
  ]
  ++
  Prelude.zip (["M1-" <> [n] | n <- ['1'..'9']]) (map (withNthWorkspace W.greedyView) [0..])
  ++
  [ ("M1-0", windows $ W.greedyView $ myWorkspaces !! 9)
  , ("M1-d", windows $ W.greedyView $ myWorkspaces !! 10)
  , ("M1-v", selectWorkspace def)
  -- | TODO: need to find a way to send there stuff
  -- , ("M-w-v", withWorkspace def (windows . W.shift))
  , ("M1-S-<Backspace>", removeWorkspace)]
  ++
  [
    ("M1-z",              windows W.focusMaster)
  , ("M1-S-z",            windows W.swapMaster)
  , ("M1-k",              windows W.focusUp)
  , ("M1-j",              windows W.focusDown)
  , ("M1-S-k",            windows W.swapUp)
  , ("M1-S-j",            windows W.swapDown)
  , ("M1-l",              sendMessage Expand)
  , ("M1-h",              sendMessage Shrink)
  , ("M1-<Backspace>", promote)
  ]
  ++
  Prelude.zip (["M1-" <> "S-" <> [n] | n <- ['1'..'9']]) (map (withNthWorkspace W.shift) [0..])
  ++
  [ ("M1-S-0",  windows $ W.shift $ myWorkspaces !! 9)
  , ("M1-S-d",  windows $ W.shift $ myWorkspaces !! 10)]
  ++
  [ ("M1-<Return>", spawn myTerminal)
  , ("M1-b", spawn myBrowser)
  , ("M-M1-h", spawn (myTerminal ++ " -e htop"))
  , ("M-M1-b", spawn (myTerminal ++ " -e btop"))
  , ("M-M1-n", spawn (myTerminal ++ " -e nvtop"))]
  ++
  [ ("M1-.", nextScreen)
  , ("M1-,", prevScreen)]
  ++
  [ ("M1-<Tab>",     sendMessage NextLayout)
  -- , ("M-S-m",   sendMessage FocusedNextLayout)
  , ("M1-C-f",   sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)]
  ++
  [ ("C-M1-j",  sendMessage MirrorShrink)
  , ("C-M1-k",  sendMessage MirrorExpand)]
  ++
  [ ("M-M1-j",  decWindowSpacing 4)
  , ("M-M1-k",  incWindowSpacing 4)
  , ("M-M1-h",  decScreenSpacing 4)
  , ("M-M1-l",  incScreenSpacing 4)]

  -- Increase/decrease windows in the master pane or the stack
  ++
  [ ("M1-S-<Up>",  sendMessage (IncMasterN 1))
  , ("M1-S-<Down>",  sendMessage (IncMasterN (-1)))
  , ("M1-=", increaseLimit)
  , ("M1--", decreaseLimit)]

  -- Sublayouts
  -- This is used to push windows to tabbed sublayouts, or pull them out of it.
  ++
  -- [ ("M-C-h",  sendMessage $ pullGroup L)
  -- , ("M-C-l",  sendMessage $ pullGroup R)
  -- , ("M-C-k",  sendMessage $ pullGroup U)
  -- , ("M-C-j",  sendMessage $ pullGroup D)
  -- , ("M-C-m",  withFocused ( sendMessage . MergeAll))
  -- -- , ("M-C-u",  withFocused (sendMessage . UnMerge))
  -- , ("M-C-/",   withFocused (sendMessage . UnMergeAll))
  [ ("M1-C-.",   onGroup W.focusUp')
  , ("M1-C-,",   onGroup W.focusDown')]

  -- Scratchpads
  -- Toggle show/hide these programs. They run on a hidden workspace.
  -- When you toggle them to show, it brings them to current workspace.
  -- Toggle them to hide and it sends them back to hidden workspace (NSP).
  ++
  [ ("M1-s t",  namedScratchpadAction myScratchPads "terminal")
  , ("M1-s b",  namedScratchpadAction myScratchPads "browser")
  , ("M1-s f",  namedScratchpadAction myScratchPads "files")
  , ("M1-s n",  namedScratchpadAction myScratchPads "clock")
  , ("M1-s c",  namedScratchpadAction myScratchPads "calculator")]

  -- Emacs (SUPER-e followed by a key)
  ++
  [
    ("M1-e e",  spawn myEmacs)
  ]

  -- Multimedia Keys
  ++
  [ ("<XF86AudioMute>",  spawn "amixer set Master toggle")
  , ("<XF86AudioLowerVolume>",  spawn "amixer set Master 5%- unmute")
  , ("<XF86AudioRaiseVolume>",  spawn "amixer set Master 5%+ unmute")
  , ("<XF86Calculator>",  runOrRaise "qalculate-gtk" (resource =? "qalculate-gtk"))
  , ("<XF86Eject>",  spawn "eject /dev/cdrom")
  , ("<Print>",  spawn "dm-maim")
  ]
  -- The following lines are needed for named scratchpads.
    where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
          nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
