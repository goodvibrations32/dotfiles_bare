import XMonad

import XMonad.Prelude(concat)
import System.Directory
import XMonad.Actions.MouseResize

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Sequence
import Data.Monoid
import Data.Tree
import qualified Data.Map as M

    -- Hooks
-- import XMonad.Hooks.DynamicLog (dynamicLogWithPP, dzen, wrap, xmobarPP, xmobarColor, shorten, PP(..), dynamicLog)
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks
    ( avoidStruts, docks, manageDocks, ToggleStruts(..), avoidStruts )
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat, doSink, isDialog)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.UrgencyHook

import qualified XMonad.Layout.LayoutModifier
    -- Layouts
-- import XMonad.Layout.LayoutCombinators hiding ((|||))
import XMonad.Layout.PerWorkspace ( onWorkspace )
-- import XMonad.Layout.SimplestFloat
import XMonad.Layout.ResizableTile ( ResizableTall(ResizableTall) )
import XMonad.Layout.Tabbed
    ( def,
      Theme(inactiveTextColor, fontName, activeColor, inactiveColor,
            activeBorderColor, activeBorderWidth, inactiveBorderColor,
            inactiveBorderWidth, activeTextColor),
      shrinkText,
      addTabs,
      tabbed,
      simpleTabbed)
import XMonad.Layout.AutoMaster ()
import XMonad.Layout.CenteredMaster (centerMaster)

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier ( ModifiedLayout )
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS, FULL))
import XMonad.Layout.NoBorders
import XMonad.Layout.Gaps
import XMonad.Layout.Renamed ( Rename(Replace), renamed, named )
import XMonad.Layout.CenterMainFluid
import XMonad.Layout.TwoPane
import XMonad.Layout.Master
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger
import XMonad.Layout.TallMastersCombo
import XMonad.Layout.Combo
import XMonad.Layout.WindowNavigation ( windowNavigation )

-- import XMonad.Actions.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))


   -- Utilities
import XMonad.Util.EZConfig (additionalKeysP, mkNamedKeymap, mkKeymap)
import XMonad.Util.Loggers
-- import XMonad.Util.NamedActions
import XMonad.Util.NamedScratchpad
import XMonad.Util.WorkspaceCompare
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.ClickableWorkspaces
import XMonad.Util.Hacks as Hacks

import Keys.MyKeys
import Generics.MyWsp
import Generics.Defaults
import Colors.ZenburnDark
-- import Layouts.MyLayouts
import XMonad.Hooks.DynamicLog (xmobarRaw)
import Language.Haskell.TH.Datatype (Strictness(Lazy))
import qualified Data.Sequences as Lazy
-- import Keys.myKeys

-- import XMonad.Actions.DynamicProjects
import XMonad.Actions.DynamicProjects (Project (projectName, projectDirectory, projectStartHook, Project), dynamicProjects)
-- import Data.Isomorphism (Iso(project))
import Basement.Compat.Base (Maybe(Just))
import XMonad.Layout.Reflect (reflectHoriz)
import XMonad.Util.SpawnOnce (spawnOnce)

myStartupHook :: X ()
myStartupHook = do
  spawn "killall trayer"  -- kill current trayer on each restart
  spawnOnce "lxsession"
  spawnOnce "xscreensaver --no-splash"
  spawnOnce "volumeicon"
  spawnOnce "blueberry-tray"
  -- spawnOnce "stalonetray --dockapp-mode none --icon-size=16"
  spawn ("sleep 2 && trayer --edge bottom --align right --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 1.0 " ++ colorTrayer ++ " --height 22")
  spawnOnce "/usr/bin/emacs --daemon"
  spawnOnce "boil"   -- if you prefer nitrogen to feh
  spawn "setxkbmap -layout us,gr -option 'grp:alt_space_toggle'" --change keyboard layouts
  spawn "gromit-mpx"
  spawn "flameshot"
  setWMName "xmonad"

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
  . concat $
  [
    [(className =? x <||> title =? x <||> resource =? x) --> doFloat | x <- myFloats]
    , [(className =? x <||> title =? x <||> resource =? x) --> doCenterFloat | x <- myCFloats]
    , [(className =? x <||> title =? x <||> resource =? x) --> doShift (myWorkspaces !! 0x5) | x <- myChat]

    , [(className =? "Mozilla Firefox" <&&> resource =? "Dialog") --> doCenterFloat]  -- Float Firefox Dialog
    , [className =? "Nyxt"                 --> doFloat]
    , [className =? "KeePassXC"            --> doShift ( myWorkspaces !! 9 )]
    , [className =? "Brave-browser"        --> doShift ( myWorkspaces !! 2 )]
    ]
  where
    myCFloats   = ["firefox", "Ranger", "Clocker"]
    myChat      = ["Signal", "Messenger", "discord", "viber"]
    myFloats    = ["confirm" , "file_progress" , "dialog" , "download"
                  , "error" , "notification" , "pinentry-gtk-2" , "splash"
                  , "toolbar"
                  ]

--Makes setting the spacingRaw simpler to write. The spacingRaw module adds a configurable amount of space around windows.
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Defining a bunch of layouts, many that I don't use.
-- limitWindows n sets maximum number of windows displayed for layout.
-- mySpacing n sets the gap size around the windows.
tall      = renamed [Replace "tall"]
            $ limitWindows 5
            $ windowNavigation
            $ mySpacing 16
            -- $ gaps [(U, 6),(D, 6),(L, 6),(R, 6)]
            $ ResizableTall 1 (3/100) (6/10) []

masterTabbedStd = renamed [Replace "masterTabbedStd"]
                  $ avoidStruts
                  -- $ gaps [(U, 4*2),(D, 4*2),(L, 2*2),(R, 2*2)]
                  $ multimastered 2 (40/100) (3/4)
                  -- $ ResizableTall 1 (3/100) (6/10) []
                  $ windowNavigation
                  $ gaps [(U, 69),(D, 69),(L, 2*4),(R, 8)]
                  $ tabbed shrinkText myTabTheme

smartTallTabbed = renamed [Replace "Smart Tall-Tabbed"]
                  $ avoidStruts
                  $ tmsCombineTwoDefault (Tall 2 (3/100) (6/10)) simpleTabbed
                  -- $  ||| tall2 ||| tabs
        -- where
        --   smartTabbed = renamed [Replace "smartTabbed"]
        --                 $ tabbed shrinkText myTabTheme


tall2      = renamed [Replace "tall2"]
            $ limitWindows 4
            $ windowNavigation
            $ mySpacing 4
            $ ResizableTall 2 (25/100) (8/10) []

tabs       = renamed [Replace "tabs"]
             $ windowNavigation
             $ noBorders
             $ tabbed shrinkText myTabTheme

multiTask  = renamed [Replace "multiTask"]
             -- $ noBorders
             $ windowNavigation
             $ addTabs shrinkText myTabTheme
             $ subLayout [] Simplest
             $ mySpacing 0
             $ ResizableTall 2 (25/100) (3/5) []

-- setting colors for tabs layout and tabs sublayout.
myTabTheme = def { fontName            = myFont
                 , activeColor         = color07
                 , inactiveColor       = "#171717"
                 , activeBorderColor   = color07
                 , activeBorderWidth   = 2
                 , inactiveBorderColor = colorBack
                 , inactiveBorderWidth = 2
                 , activeTextColor     = colorBack
                 , inactiveTextColor   = color16
                 }

dynproj :: [Project]
dynproj =
  [ Project
      { projectName = "chat",
        projectDirectory = "~/",
        projectStartHook = Just $ do
          spawn "discord"
          spawn "signal-desktop"
          spawn "chromium  -new-window https://www.facebook.com/messages/e2ee/t/7553229908054801 --class='Messenger'"
      }
  , Project
        { projectName = "dev",
          projectDirectory = "~/",
          projectStartHook = Just $ do
            spawn "emacs"
            spawn "alacritty -e tmux new -As0"
            spawn "/home/toro/xmonadhack"
        }
  -- , Project
  --       { projectName = "sys",
  --         projectDirectory = "~/",
  --         projectStartHook = Just $ do
  --           spawn "alacritty -e nvtop"
  --           spawn "alacritty -e htop"
  --           spawn "alacritty -e nload wlan0"
  --       }
  ]

-- The layout hook
myLayoutHook = avoidStruts
               $ mouseResize
               $ windowArrange
               $ onWorkspace "chat" tabs
               $ onWorkspace "sys" masterTabbedStd
               $ onWorkspace "dev" tall2
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
               -- $ mkToggle (NOBORDERS ?? FULL ?? EOT) myDefaultLayout
  where
    myDefaultLayout = withBorder myBorderWidth tall
                                                    XMonad.||| CenterMainFluid 1 (3/0x64) (70/0x64)
                                                    XMonad.||| multiTask
                                                    XMonad.||| masterTabbedStd
                                                    XMonad.||| smartTallTabbed

myBar :: PP
myBar = def
        { ppSep             = purple " • "
        , ppTitle           = wrap "[" "]" . ppWindow
        , ppTitleSanitize   = xmobarStrip
        , ppCurrent         = purple . wrap "{" "}"
        , ppVisible         = xmobarColor color03 "" . wrap " " " "
        , ppHidden          = hiddenCol . wrap " " " "
        , ppUrgent          = red . wrap (yellow "!") (yellow "!")
        , ppOrder = \(ws : l : _ : wins : _) -> [ws, l, wins]
        , ppExtras  = [logTitles formatFocused formatUnfocused]
        }
         where
           formatUnfocused  = unfocCol . wrap "<<" ">>" . ppWindow
           formatFocused  = purple . wrap "<|" "|>" . ppWindow

purple, lowWhite, red, hiddenCol, yellow :: String -> String
purple        = xmobarColor "#8787AF" ""
hiddenCol     = xmobarColor color06   ""
yellow        = xmobarColor "#f1fa8c" ""
red           = xmobarColor "#ff5555" ""
lowWhite      = xmobarColor "#bbbbbb" ""
unfocCol      = hiddenCol

ppWindow :: String -> String
ppWindow = xmobarRaw . (\w -> if Prelude.null w then "untitled" else w) . shorten 30

myConf = def
    { manageHook         = myManageHook
                           <+> manageDocks
                           <+> namedScratchpadManageHook myScratchPads
                           <+> (isFullscreen -->  doFullFloat)
                           <+> (isDialog --> doCenterFloat)
    , handleEventHook    = handleEventHook def
                           <> Hacks.trayerPaddingXmobarEventHook
                           <> Hacks.fixSteamFlicker
    , modMask            = myModMask
    , terminal           = myTerminal
    , layoutHook         = myLayoutHook
    , workspaces         = myWorkspaces
    , startupHook        = myStartupHook
    , borderWidth        = myBorderWidth
    , normalBorderColor  = myNormColor
    , focusedBorderColor = myFocusColor
    }


-- Filter the NSP
myFilter = filterOutWs [scratchpadWorkspaceTag]

main :: IO ()
main = do
  xmonad
    -- . ewmhFullscreen
    $ dynamicProjects dynproj
    . withSB (statusBarProp "xmobar" (clickablePP myBar))
    . setEwmhActivateHook doAskUrgent
    . ewmh
    . docks
    . withUrgencyHook NoUrgencyHook
    $ myConf
    `additionalKeysP` myEmKeys ++ [("M-f", sendMessage $ JumpToLayout "Full")]
