;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;;; Name and mail
(setq user-full-name "Torosian Nikolas"
      user-mail-address "goodvibrations32@protonmail.com")
(setq erc-server "colonq.computer"
      erc-port "26697"
      erc-nick "goodvibrations32"
      erc-user-full-name user-full-name
      erc-track-shorten-start 8
      erc-kill-buffer-on-part t)
(setq doom-scratch-initial-major-mode 'lisp-interaction-mode)

;;; Doom fonts
(setq doom-font (font-spec :family "Hasklug Nerd Font Mono" :weight 'semibold)
      doom-variable-pitch-font (font-spec :family "Hasklug Nerd Font" :height 840)
      doom-big-font (font-spec :family "Hasklug Nerd Font Mono" :size 24)
      ;; doom-symbol-font (font-spec :family "Hasklug Nerd Font Mono" :size 30)
      doom-symbol-font nil
      )

(remove-hook '+popup-buffer-mode-hook #'+popup-set-modeline-on-enable-h)
(after! info
  (set-popup-rule! "\\*info" :size 0.40 :vslot -4 :side 'right))
  ;; (set-popup-rule! "\\*compilation" :size 0.40 :vslot -4 :side 'right)

;; (after! embark
;;   (set-popup-rule! "\\*Embark" :size 0.30 :vslot -4 :side 'right))

;; (setq initial-buffer-choice "~/.doom.d/config.el")
;;;;;; Terminal executables for eshell and vterm
(setq shell-file-name (executable-find "bash"))
(setq-default vterm-shell (executable-find "zsh"))
(after! vterm
  (set-popup-rule! "\\*doom:vterm-popup" :size 0.40 :vslot -4 :side 'right))

;;;;; THEME
(load-theme 'doom-ir-black t)
;; (add-hook 'emacs-startup-hook #'doom/open-scratch-buffer)

;;;;; tree-sitter
(after! tree-sitter
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode t)
  (tree-sitter-hl-mode t)
)
(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)
;; (tree-sitter-require 'clang)


;;;;; completion settings
(setq completion-styles '(orderless basic))

(after! corfu
  :config
  (setq corfu-auto-delay 0.01
        corfu-popupinfo-delay '(0.1 . 0.1))
  (global-corfu-mode t))
(defun t/eglot-config ()
  "This is used to turn all unwanted modes from eglot off."
  (custom-set-faces '(eglot-mode-line ((t))))
  (setq doom-modeline-vcs-name nil)
  (eglot-inlay-hints-mode -1))
(add-hook 'eglot-managed-mode-hook #'t/eglot-config)

;; (use-package! focus)

;;;; Custom commands...
;;;;; buffers
(map! :leader
      :desc "make the buffer dance"
      :prefix "b"
      :desc "List bookmarks" "L" #'list-bookmarks
      :desc "previous buffer" "M-p" #'previous-buffer
      :desc "next buffer" "M-n" #'next-buffer)

;;;;;; Revert buffers when changed (make it auto!!!)
(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)
(global-set-key (kbd "C-u") (kbd "C-u z z"))
(global-set-key (kbd "C-d") (kbd "C-d z z"))

;;;;;; I-buffer keybindings
(evil-define-key 'normal ibuffer-mode-map
  (kbd "f c") 'ibuffer-filter-by-content
  (kbd "f d") 'ibuffer-filter-by-directory
  (kbd "f f") 'ibuffer-filter-by-filename
  (kbd "f m") 'ibuffer-filter-by-mode
  (kbd "f n") 'ibuffer-filter-by-name
  (kbd "f x") 'ibuffer-filter-disable
  (kbd "g h") 'ibuffer-do-kill-lines
  (kbd "g H") 'ibuffer-update)

;;;;; dired
;;;;; keybinds and config
(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-find-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-do-chmod
  (kbd "O") 'dired-do-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-do-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt
  (kbd "; e") 'epa-dired-do-encrypt)
;; Get file icons in dired
(defun t/dired-init ()
  "to be run as hook for `dired-mode'."
  (dired-hide-details-mode -1)
  (dired-omit-mode -1))

(add-hook 'dired-mode-hook 't/dired-init)

(setq delete-by-moving-to-trash t
      trash-directory "~/.local/share/Trash/files/")

;; (setq global-display-line-numbers-mode nil)
(setq display-line-numbers-type nil)
;;;; Custom commands...
;;;;;; Toggle ops
(map! :leader
      :desc "Comment or uncomment lines" "DEL" #'comment-line
      :prefix "t"
      :desc "Toggle line numbers" "l" #'doom/toggle-line-numbers
      :desc "Toggle line highlight in frame" "h" #'hl-line-mode
      :desc "Toggle line highlight globally" "H" #'global-hl-line-mode
      :desc "Toggle truncate lines" "t" #'toggle-truncate-lines
      :desc "Ripgrep consult" "r" #'consult-ripgrep
      :desc "Toogle heagers numbering" "a" #'org-num-mode)

;;;;;; consult commands to space j
(map! :leader
      :prefix "j"
      :desc "Consult jump to file" "j" #'evil-collection-consult-jump-list
      :desc "Consult open on the side" "o" #'consult-buffer-other-window)

(map! :leader
      :desc "Org babel tangle" "m B" #'org-babel-tangle)

;;;; Configuring org mode
(after! org
  (setq org-directory "~/org/"
        org-agenda-files '("~/org/agenda.org"
                           "~/org/diss.org"
                           "~/org/lab.org"
                           "~/org/music.org"
                           "~/org/work.org")
        org-ellipsis "↴"
        org-log-done 'note
        ;; org-num-mode 1 need to find a better way... for now map it on a binding
        org-hide-emphasis-markers t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
        '(("google" . "http://www.google.com/search?q=")
          ("arch-wiki" . "https://wiki.archlinux.org/index.php/")
          ("ddg" . "https://duckduckgo.com/?q=")
          ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-table-convert-region-max-lines 20000
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
        '((sequence
           "TODO(t)" "UNIV(u)" "PROJ(p)" "HOME(h)" "WAIT(w)" "MODS(m)" "ASK(s)"
           "DELEGATED(e)" "LOOP(l)" "KILL(k)" "|" "DONE(d)" "CANCELLED(c)")))
  (setq org-agenda-start-with-log-mode t
        org-agenda-start-on-weekday nil
        org-agenda-start-day "+0d"
        org-agenda-span 1
        org-log-into-drawer t
        org-agenda-include-diary nil
        org-deadline-warning-days 35
        org-agenda-time-grid '((daily today require-timed)
                              (800 1000 1200 1400 1600 1800 2000)
                              " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
        org-deadline-time-hour-regexp t
        org-agenda-block-separator 126
        org-agenda-compact-blocks nil)

  (setq org-agenda-custom-commands
        '(("h" "tasks Home-related"
           ((tags-todo "home")
            (tags-todo "voula")
            (tags "garden")))
          ("d" "Upcoming deadlines" agenda ""
           ((org-agenda-time-grid nil)
            (org-deadline-warning-days 365)
            (org-agenda-entry-types '(:deadline))))
          ("v" "Big agenda view"
           ((tags "PRIORITY=\"A\""
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if
                      ;; 'todo
                      'done))
                   (org-agenda-overriding-header
                    "High-priority unfinished tasks:")))
            (tags "PRIORITY=\"a\""
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if
                      'todo))
                   (org-agenda-overriding-header
                    "Mid-priority unfinished tasks:")))
            (tags "PRIORITY=\"C\""
                  ((org-agenda-skip-function
                    '(org-agenda-skip-entry-if
                      'todo
                      'done))
                   (org-agenda-overriding-header
                    "Low-priority unfinished tasks:")))))
          ("w" "tasks Work-related"
           ((tags-todo "lab")
            (tags-todo "Wflow")
            (tags-todo "music"))))
        )
  (setq org-habit-following-days 7
        org-habit-preceding-days 35
        org-habit-show-habits t
        org-habit-show-all-today t
        org-habit-graph-column 90))
(custom-set-faces
 '(Info-quoted ((t (:inherit variable-pitch))))
 '(org-level-1 ((t (:inherit outline-1 :height 1.6 :bold t))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.5 :bold t))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.4 :bold t))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.2))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.0)))))

(defun t/diary-sunrise ()
  "This will make the sunrise what i want in agenda using lat longitude."
  (let ((dss (diary-sunrise-sunset)))
    (save-match-data (string-match "^Sunrise \\([^ ]*\\) " dss)
                     (concat "🌅 " (match-string 1 dss)))))
(defun t/diary-sunset ()
  "This will make the sunset what i want in agenda using lat longitude."
  (let ((dss (diary-sunrise-sunset)))
    (save-match-data (string-match ", sunset \\([^ ]*\\) " dss)
                     (concat "🌇 " (match-string 1 dss)))))
(defvar calendar-latitude 38.05)
(defvar calendar-longitude 23.4)
(setq calendar-mark-diary-entries-flag t)
(add-hook! 'org-finalize-agenda-hook
          (setq appt-message-warning-time 10        ;; warn 10 min in advance
                appt-display-diary nil              ;; do not display diary when (appt-activate) is called
                appt-display-mode-line t            ;; show in the modeline
                appt-display-format 'window         ;; display notification in window
                display-fill-column-indicator-mode 0
                calendar-mark-diary-entries-flag t) ;; mark diary entries in calendar
           (org-agenda-to-appt)                      ;; copy all agenda schedule to appointments
           (appt-activate 1))                       ;; active appt (appointment notification)

;;;;; Org-mode setup for agenda capture-templates and more
(setq org-capture-templates
      `(("t" "tasks / projects")
        ("tt" "task" entry (file+olp "~/org/agenda.org")
         "* TODO %?\n %U\n %a\n %i" :empty-lines 1)
        ("tp" "projects" entry (file+olp "~/org/agenda.org")
         "* Proj  %?\n %U\n %a\n %i" :empty-lines 1)

        ("h" "habits")
        ("hh" "Habit template" entry (file+olp+datetree "~/org/agenda.org")
         "* TODO %?
        :PROPERTIES:
        :STYLE: habit
        :LOGBOOK:
        :END:")
        ("l" "Lab")
        ("lt" "new task" entry
         (file+olp+datetree "~/org/lab.org")
         "* TODO %? :lab:tasks: \n%U"
         :empty-lines 0)
        ("ll" "new linked task" entry
         (file+olp+datetree "~/org/lab.org")
         "* TODO %? :lab:tasks: \n%U \n%A"
         :empty-lines 0)
        ("lr" "Music Note" entry
         (file+olp "~/org/music.org" "Notes")
         "* TODO %? :lab:music: \n%U \n%A"
         :empty-lines 0)
        ("lm" "Music" entry
         (file+olp+datetree "~/org/lab.org")
         "* TODO %? :lab:music: \n%U \n%A"
         :empty-lines 0)
        ("ls" "Meeting" entry
         (file+olp+datetree "~/org/lab.org")
         "* ~ With %? :lab:meetings: \n %^{SCHEDULED}p%^{TIMESTAMP|filed:%U}"
         :empty-lines 0)

        ("w" "Workflows")
        ;; ("wd" "Dissertation" entry
        ;;  (file+olp+datetree "~/org/diss.org")
        ;;  "\n* %<I%H:%M %p> - :Wflow:\n %a \n%?\n\n"
        ;;  :empty-lines 1)

        ;; ("wn" "Dissertation-Doc-notes" entry
        ;;  (file+olp+datetree "/home/toro/Documents/dissertation/org-docs/diss-docs/main_doc/notes-diss.org")
        ;;  "\n* %<I%H:%M %p> for eval - :Wflow:\n %a \n%?\n\n"
        ;;  :empty-lines 1)

        ("wg" "general work" entry
         (file+olp+datetree "~/org/work.org")
         "\n* %<I%H:%M %p> - %a :Wflow:\n\n%?\n\n"
         :clock-in :clock-resume
         :empty-lines 1)

        ("m" "Metrics Capture")
        ("mw" "Workout" table-line (file+headline "~/org/metrics.org" "Workout")
         "| %U | %^{time} | %^{notes}" :kill-buffer t)))


(setq org-refile-targets
      '(("~/org/archive.org" :maxlevel . 1)))
(advice-add 'org-refile :after 'org-save-all-org-buffers)

;; (setq diary-european-date-forms 1)
(setq calendar-date-style 'european)



(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 60 t)) ; make this as low as needed

(add-hook 'markdown-mode-hook 'prefer-horizontal-split)

;; (map! :leader
;;       :desc "Clone indirect buffer other window" "b c" #'clone-indirect-buffer-other-window)



;;;; Resize windows without hasle
;; Vertical resize in each direction via +/-
(defun t/v-resize (key)
  "interactively resize the window"
  (interactive "cHit +/- to verdical enlarge/shrink")
  (cond
   ((eq key (string-to-char "="))
    (enlarge-window 8)
    (call-interactively 't/v-resize))
   ((eq key (string-to-char "-"))
    (enlarge-window -8)
    (call-interactively 't/v-resize))
   (t (push key unread-command-events))))
(map! :leader
      :prefix "w"
      :desc "Resize window (=/-) vertical" "2" #'t/v-resize)

;; Horizontal window resize in each direction via +/-
(defun t/h-resize (key)
  "interactively resize the window"
  (interactive "cHit =/- to horizontal enlarge/shrink")
  (cond
   ((eq key (string-to-char "="))
    (enlarge-window-horizontally 8)
    (call-interactively 't/h-resize))
   ((eq key (string-to-char "-"))
    (enlarge-window-horizontally -8)
    (call-interactively 't/h-resize))
   (t (push key unread-command-events))))
(map! :leader
      :prefix "w"
      :desc "Resize window (+/-) horizontal" "1" #'t/h-resize)

;;;; this seems to add syntax-highlighting to jupyter-python
;; (after! org-src
;;   (dolist (lang '(python typescript jupyter rust))
;;     (cl-pushnew (cons (format "jupyter-%s" lang) lang)
;;                 org-src-lang-modes :key #'car))
;;   )
;; (setq char-or-string-p t)

;;;;; blink cursor and org-modules
(blink-cursor-mode 0)
(with-eval-after-load 'org
  (add-to-list 'org-modules 'org-habit t)
  (add-to-list 'org-modules 'org-secretary t))

;;;;; modeline
;; ;; Whether to use hud instead of default bar. It's only respected in GUI.
(defun t/disable-shit ()
  (size-indication-mode -1)
  (line-number-mode -1)
  (column-number-mode -1))
(add-hook 'doom-init-ui-hook #'t/disable-shit)
(add-hook 'c-mode-hook (lambda () (c-toggle-comment-style -1)))

(setq! doom-modeline-hud t
       doom-modeline-project-detection 'auto
       doom-modeline-buffer-file-name-style 'truncate-upto-project
       doom-modeline-height 8
       doom-modeline-icon nil
       doom-modeline-major-mode-icon nil
       doom-modeline-check-simple-format t
       ;;(propertize (format "%s" (system-name)) 'face 'mode-line-highlight)
       doom-modeline-percent-position ""
       doom-modeline-major-mode-color-icon nil
       doom-modeline-buffer-state-icon nil
       doom-modeline-buffer-modification-icon t
       doom-modeline-time-icon nil
       doom-modeline-enable-word-count t
       ;; doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode)
       doom-modeline-buffer-encoding nil
       ;; doom-modeline-checker-simple-format t
       doom-modeline-number-limit 99
       doom-modeline-vcs-max-length 12
       ;; doom-modeline-persp-name t
       doom-modeline-display-default-persp-name t
       doom-modeline-lsp nil
       doom-modeline-modal-icon 'xah-fly-keys
       doom-modeline-modal nil
       doom-modeline-irc t
       doom-modeline-irc-stylize 'identity
       doom-modeline-env-version t
       doom-modeline-env-enable-python t
       doom-modeline-env-enable-rust t
       doom-modeline-env-python-executable "python" ; or `python-shell-interpreter'
       doom-modeline-env-rust-executable "rustc"
       doom-modeline-env-load-string "..."
       doom-modeline-before-update-env-hook nil
       doom-modeline-after-update-env-hook nil)
;; (doom-modeline-def-segment (format-mode-line ""))
;;;;; elfeed
(setq elfeed-feeds
      '(("https://thepressproject.gr/feed/" Press news)
        ("https://archlinux.org/feeds/news/" arch news)
        ("https://lexfridman.com/feed/podcast/" lex)
        ("https://github.com/doomemacs/doomemacs/commits/master.atom" doom news)
        ;; ("https://archlinux.org/feeds/packages/x86_64/core/" core news)
        ))

;;;;; latex
(setq org-image-actual-width 500)
(setq org-latex-images-centered t)
(setq reftex-default-bibliography "~/Documents/dissertation/org-docs/bib-files/*.bib")

;; (add-hook 'dired-mode-hook (lambda () (text-scale-increase 4)))

;; An atempt to make latex beautiful
;; (setq org-latex-compiler "xetex")
(use-package! pdf-tools
  :defer t
  :config
  (pdf-tools-install)
  (setq-default pdf-view-display-size 'fit-width))
(add-hook 'pdf-view-mode-hook #'pdf-view-midnight-minor-mode)

(setq org-latex-src-block-backend 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

(defun my/org-export-as-pdf-and-render ()
  (interactive)
  (org-timer-start)
  (save-buffer
   (org-latex-export-to-pdf))
  (org-timer-pause-or-continue)
  (let ((default-directory "/tmp"))
    (shell-command "notify-send -u critical 'Done with org -> pdf'")))
(evil-define-key 'normal org-mode-map
  (kbd "<f6>") 'my/org-export-as-pdf-and-render)

;;; reveal js for presentation
;; reveal.js variables
;; (require 'ox-reveal)
;; (setq org-reveal-root "./reveal.js")
;; ;; (setq org-reveal-root "/home/dtos_experiment/reveal.js")
;; (setq org-reveal-title-slide nil)
;; ;; (setq org-re-reveal-title-slide nil)
;; (defun my/org-reveal-export-to-html ()
;;   (interactive)
;;   (save-buffer
;;    (org-reveal-export-to-html)))
;; (evil-define-key 'normal org-mode-map
;;   (kbd "<f5>") 'my/org-reveal-export-to-html)


(setq auth-sources '("~/.authinfo"))

;;;; Faces
;; (set-face-attribute 'region nil :background "#666" :foreground "#d79921")
(setq evil-emacs-state-cursor '("#d3869b" box))
;; (setq evil-normal-state-cursor '("#ebdbb2" box))
;; (setq evil-visual-state-cursor '("#6CA0A3" hollow ))
(setq evil-insert-state-cursor '("#6F8F6F" box))
;; (setq evil-replace-state-cursor '("red" hollow))
(setq evil-operator-state-cursor '("red" underline))

;; (setq scroll-conservatively 69)
;; (setq scroll-step 35)
;; (setq scroll-down-aggressively 0.9)
;; (set-frame-parameter nil 'alpha-background 100)

;; (add-to-list 'default-frame-alist '(alpha-background . 0.95))

;;;;;; spelling
(after! flyspell-lazy
  (flyspell-lazy-mode -1))

(setq split-width-threshold 80
      split-height-threshold nil)
(add-hook 'flycheck-mode-hook #'flycheck-cask-setup)
(setq org-element-use-cache nil)

;;;; Outli
(use-package outli
  :defer t
  :load-path "/home/toro/.doom.d/outli"
  ;; :after lispy ; uncomment only if you use lispy; it also sets speed keys on headers!
  ;; :bind (:map outli-mode-map
  ;;             ("C-c C-p" . (lambda () (interactive) (outline-back-to-heading))))
  :hook ((prog-mode elisp-mode) . outli-mode)) ; or whichever modes you prefer

;;;; magit config
(after! magit-todos
  ;; :after magit
  :config
  (magit-todos-mode 1))

;;;; find SSH_AGENT on the sys
(exec-path-from-shell-copy-env "SSH_AGENT_PID")
(exec-path-from-shell-copy-env "SSH_AUTH_SOCK")

(setq haskell-process-type 'cabal-new-repl)

(setq gdb-many-windows t)

(setq-default windows-size-fixed t)
(load-file "~/personal/learningElisp/midi-calculator.el")

;;;; test indentation
(setq indent-tabs-mode nil)
(setq! c-basic-offset 4)
