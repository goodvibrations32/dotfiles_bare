# "  ____ _____
# " |   \   |  Derek Taylor (DistroTube)
# " | | | || |    http://www.youtube.com/c/DistroTube
# " | || || |    http://www.gitlab.com/dwt1/
# " |____/ |_|
# "
# " A customized .vimrc for vim (https://www.vim.org/)

set nocompatible
filetype off

call plug#begin('~/.vim/plugged')
Plug 'suan/vim-instant-markdown', {'rtp': 'after'}
Plug 'frazrepo/vim-rainbow'
Plug 'vifm/vifm.vim'
Plug 'vimwiki/vimwiki'
Plug 'jreybert/vimagit'
Plug 'tpope/vim-surround'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'kovetskiy/sxhkd-vim'
Plug 'vim-python/python-syntax'
Plug 'ap/vim-css-color'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'junegunn/vim-emoji'

call plug#end()

filetype plugin indent on
# " To ignore plugin indent changes, instead use:
# " filetype plugin on

# " Brief help
# " :PluginList       - lists configured plugins
# " :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
# " :PluginSearch foo - searches for foo; append `!` to refresh local cache
# " :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

# " see :h vundle for more details or wiki for FAQ
# " Put your non-Plugin stuff after this line

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => General Settings
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set incsearch
set hidden
set nobackup
set noswapfile
set t_Co=256
set number relativenumber
set clipboard=unnamedplus
syntax enable
let g:rehash256 = 1

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Status Line
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " The lightline.vim theme
let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ }

# " Always show statusline
set laststatus=2

# " Uncomment to prevent non-normal modes showing in powerline and below powerline.
set noshowmode

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Text, tab and indent related
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Colors and Theming
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
highlight LineNr           ctermfg=8    ctermbg=none    cterm=none
highlight CursorLineNr     ctermfg=7    ctermbg=8       cterm=none
highlight VertSplit        ctermfg=0    ctermbg=8       cterm=none
highlight Statement        ctermfg=2    ctermbg=none    cterm=none
highlight Directory        ctermfg=4    ctermbg=none    cterm=none
highlight StatusLine       ctermfg=7    ctermbg=8       cterm=none
highlight StatusLineNC     ctermfg=7    ctermbg=8       cterm=none
highlight NERDTreeClosable ctermfg=2
highlight NERDTreeOpenable ctermfg=8
highlight Comment          ctermfg=4    ctermbg=none    cterm=italic
highlight Constant         ctermfg=12   ctermbg=none    cterm=none
highlight Special          ctermfg=4    ctermbg=none    cterm=none
highlight Identifier       ctermfg=6    ctermbg=none    cterm=none
highlight PreProc          ctermfg=5    ctermbg=none    cterm=none
highlight String           ctermfg=12   ctermbg=none    cterm=none
highlight Number           ctermfg=1    ctermbg=none    cterm=none
highlight Function         ctermfg=1    ctermbg=none    cterm=none
# " highlight WildMenu         ctermfg=0       ctermbg=80      cterm=none
# " highlight Folded           ctermfg=103     ctermbg=234     cterm=none
# " highlight FoldColumn       ctermfg=103     ctermbg=234     cterm=none
# " highlight DiffAdd          ctermfg=none    ctermbg=23      cterm=none
# " highlight DiffChange       ctermfg=none    ctermbg=56      cterm=none
# " highlight DiffDelete       ctermfg=168     ctermbg=96      cterm=none
# " highlight DiffText         ctermfg=0       ctermbg=80      cterm=none
# " highlight SignColumn       ctermfg=244     ctermbg=235     cterm=none
# " highlight Conceal          ctermfg=251     ctermbg=none    cterm=none
# " highlight SpellBad         ctermfg=168     ctermbg=none    cterm=underline
# " highlight SpellCap         ctermfg=80      ctermbg=none    cterm=underline
# " highlight SpellRare        ctermfg=121     ctermbg=none    cterm=underline
# " highlight SpellLocal       ctermfg=186     ctermbg=none    cterm=underline
# " highlight Pmenu            ctermfg=251     ctermbg=234     cterm=none
# " highlight PmenuSel         ctermfg=0       ctermbg=111     cterm=none
# " highlight PmenuSbar        ctermfg=206     ctermbg=235     cterm=none
# " highlight PmenuThumb       ctermfg=235     ctermbg=206     cterm=none
# " highlight TabLine          ctermfg=244     ctermbg=234     cterm=none
# " highlight TablineSel       ctermfg=0       ctermbg=247     cterm=none
# " highlight TablineFill      ctermfg=244     ctermbg=234     cterm=none
# " highlight CursorColumn     ctermfg=none    ctermbg=236     cterm=none
# " highlight CursorLine       ctermfg=none    ctermbg=236     cterm=none
# " highlight ColorColumn      ctermfg=none    ctermbg=236     cterm=none
# " highlight Cursor           ctermfg=0       ctermbg=5       cterm=none
# " highlight htmlEndTag       ctermfg=114     ctermbg=none    cterm=none
# " highlight xmlEndTag        ctermfg=114     ctermbg=none    cterm=none

###############################################################
# " => Vifm
###############################################################
map <Leader>vv :Vifm<CR>= ' '
map <Leader>vs :VsplitVifm<CR>= ' '
map <Leader>sp :SplitVifm<CR>= ' '
map <Leader>dv :DiffVifm<CR>= ' '
map <Leader>tv :TabVifm<CR>= ' '


###############################################################
# " => VimWiki
###############################################################
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Vim-Instant-Markdown "
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:instant_markdown_autostart = 0
let g:instant_markdown_browser = "surf"
map <Leader>md :InstantMarkdownPreview<CR>
map <Leader>ms :InstantMarkdownStop<CR>

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Open terminal inside Vim
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Leader>tt :vnew term://fish<CR>

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Mouse Scrolling
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set mouse=nicr

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Splits and Tabbed Files
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright

# " Remap splits navigation to just CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

# " Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

# " Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

# " Removes pipes | that act as seperators on splits
set fillchars+=vert:\

# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# " => Other Stuff
# """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:python_highlight_all = 1

au! BufRead,BufWrite,BufWritePost,BufNewFile *.org
au BufEnter *.org            call org#SetOrgFileType()

set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions-=L
