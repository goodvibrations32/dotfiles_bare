#if 0
	shc Version 3.8.9b, Generic Script Compiler
	Copyright (c) 1994-2015 Francisco Rosales <frosal@fi.upm.es>

	shc -f /home/dtos_experiment/.config/rofi/scripts/crete_screens.sh 
#endif

static  char data [] = 
#define      tst1_z	22
#define      tst1	((&data[1]))
	"\264\054\267\351\335\347\011\221\053\200\250\042\364\062\102\062"
	"\306\057\134\171\130\210\051\012\130\331"
#define      chk1_z	22
#define      chk1	((&data[26]))
	"\347\047\206\141\315\374\234\072\136\245\207\217\057\014\350\250"
	"\071\014\047\275\326\233\304\342\232"
#define      msg2_z	19
#define      msg2	((&data[53]))
	"\205\376\232\247\350\102\267\311\124\306\044\131\146\075\327\023"
	"\156\240\113\125\157\207\050\254"
#define      rlax_z	1
#define      rlax	((&data[75]))
	"\220"
#define      msg1_z	42
#define      msg1	((&data[84]))
	"\321\353\253\341\152\367\210\037\272\366\011\226\063\215\206\151"
	"\313\353\300\005\341\246\372\333\271\045\054\046\213\013\241\051"
	"\362\242\066\066\342\042\052\100\114\365\331\230\234\167\122\035"
	"\354\030\002\341\370\225\371\225"
#define      xecc_z	15
#define      xecc	((&data[135]))
	"\341\150\327\221\241\025\177\055\301\360\262\077\161\077\162\241"
	"\135\251"
#define      lsto_z	1
#define      lsto	((&data[150]))
	"\227"
#define      opts_z	1
#define      opts	((&data[151]))
	"\354"
#define      inlo_z	3
#define      inlo	((&data[152]))
	"\301\000\003"
#define      pswd_z	256
#define      pswd	((&data[165]))
	"\367\325\346\141\315\157\200\317\120\171\343\347\327\015\023\027"
	"\041\326\007\345\333\104\163\117\251\335\050\006\321\154\320\373"
	"\201\157\250\036\213\343\155\102\262\120\052\211\136\075\240\177"
	"\024\250\145\357\354\331\077\225\266\147\234\210\324\155\204\125"
	"\335\054\163\151\020\341\254\302\061\326\113\220\024\354\017\050"
	"\224\164\030\201\116\130\027\004\300\264\215\224\041\021\352\377"
	"\076\136\150\116\077\024\021\161\353\135\001\000\111\020\051\336"
	"\205\101\140\323\232\167\330\132\053\146\356\115\167\330\114\265"
	"\066\265\004\165\312\025\346\265\162\350\266\274\370\337\232\176"
	"\041\373\122\273\162\053\025\236\221\003\354\011\334\071\277\023"
	"\356\304\211\270\331\160\156\114\130\044\010\120\004\243\317\045"
	"\236\041\340\021\115\365\257\336\371\233\350\326\324\250\351\303"
	"\154\162\174\105\342\352\222\072\017\233\213\024\076\133\071\335"
	"\175\031\356\312\017\236\251\011\072\221\337\017\071\311\322\246"
	"\073\116\353\036\071\176\131\111\031\345\135\130\100\227\065\275"
	"\260\044\210\300\303\061\311\375\303\251\015\375\162\337\243\256"
	"\056\217\314\150\015\046\262\046\013\020\144"
#define      text_z	114
#define      text	((&data[423]))
	"\046\047\122\314\214\260\324\357\135\206\311\376\134\345\263\300"
	"\072\204\363\145\260\046\361\242\014\201\357\066\214\165\017\075"
	"\115\343\140\142\340\061\165\067\052\276\341\376\232\326\003\306"
	"\235\376\211\213\074\272\010\126\043\024\214\371\357\267\053\173"
	"\101\164\034\343\064\172\071\063\362\067\372\360\355\011\016\313"
	"\047\056\270\257\106\320\052\277\136\147\312\306\054\256\137\232"
	"\074\204\231\030\340\335\341\056\225\060\032\077\035\035\360\273"
	"\025\266\136\076\240\217\025\006\365\164\225\010\210\333\224\274"
	"\347\277\300\336\225\247\100\142"
#define      date_z	1
#define      date	((&data[558]))
	"\140"
#define      tst2_z	19
#define      tst2	((&data[560]))
	"\260\147\304\217\232\354\065\212\172\253\316\102\047\301\150\176"
	"\267\141\130\014\110\126"
#define      chk2_z	19
#define      chk2	((&data[583]))
	"\146\234\232\356\127\100\175\247\312\243\271\132\054\126\212\207"
	"\362\116\237\257\025\374\134\020\222"
#define      shll_z	14
#define      shll	((&data[608]))
	"\370\125\335\277\101\207\162\146\223\106\170\363\217\353\262\205"
	"\125"/* End of data[] */;
#define      hide_z	4096
#define DEBUGEXEC	0	/* Define as 1 to debug execvp calls */
#define TRACEABLE	0	/* Define as 1 to enable ptrace the executable */

/* rtc.c */

#include <sys/stat.h>
#include <sys/types.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

/* 'Alleged RC4' */

static unsigned char stte[256], indx, jndx, kndx;

/*
 * Reset arc4 stte. 
 */
void stte_0(void)
{
	indx = jndx = kndx = 0;
	do {
		stte[indx] = indx;
	} while (++indx);
}

/*
 * Set key. Can be used more than once. 
 */
void key(void * str, int len)
{
	unsigned char tmp, * ptr = (unsigned char *)str;
	while (len > 0) {
		do {
			tmp = stte[indx];
			kndx += tmp;
			kndx += ptr[(int)indx % len];
			stte[indx] = stte[kndx];
			stte[kndx] = tmp;
		} while (++indx);
		ptr += 256;
		len -= 256;
	}
}

/*
 * Crypt data. 
 */
void arc4(void * str, int len)
{
	unsigned char tmp, * ptr = (unsigned char *)str;
	while (len > 0) {
		indx++;
		tmp = stte[indx];
		jndx += tmp;
		stte[indx] = stte[jndx];
		stte[jndx] = tmp;
		tmp += stte[indx];
		*ptr ^= stte[tmp];
		ptr++;
		len--;
	}
}

/* End of ARC4 */

/*
 * Key with file invariants. 
 */
int key_with_file(char * file)
{
	struct stat statf[1];
	struct stat control[1];

	if (stat(file, statf) < 0)
		return -1;

	/* Turn on stable fields */
	memset(control, 0, sizeof(control));
	control->st_ino = statf->st_ino;
	control->st_dev = statf->st_dev;
	control->st_rdev = statf->st_rdev;
	control->st_uid = statf->st_uid;
	control->st_gid = statf->st_gid;
	control->st_size = statf->st_size;
	control->st_mtime = statf->st_mtime;
	control->st_ctime = statf->st_ctime;
	key(control, sizeof(control));
	return 0;
}

#if DEBUGEXEC
void debugexec(char * sh11, int argc, char ** argv)
{
	int i;
	fprintf(stderr, "shll=%s\n", sh11 ? sh11 : "<null>");
	fprintf(stderr, "argc=%d\n", argc);
	if (!argv) {
		fprintf(stderr, "argv=<null>\n");
	} else { 
		for (i = 0; i <= argc ; i++)
			fprintf(stderr, "argv[%d]=%.60s\n", i, argv[i] ? argv[i] : "<null>");
	}
}
#endif /* DEBUGEXEC */

void rmarg(char ** argv, char * arg)
{
	for (; argv && *argv && *argv != arg; argv++);
	for (; argv && *argv; argv++)
		*argv = argv[1];
}

int chkenv(int argc)
{
	char buff[512];
	unsigned long mask, m;
	int l, a, c;
	char * string;
	extern char ** environ;

	mask  = (unsigned long)&chkenv;
	mask ^= (unsigned long)getpid() * ~mask;
	sprintf(buff, "x%lx", mask);
	string = getenv(buff);
#if DEBUGEXEC
	fprintf(stderr, "getenv(%s)=%s\n", buff, string ? string : "<null>");
#endif
	l = strlen(buff);
	if (!string) {
		/* 1st */
		sprintf(&buff[l], "=%lu %d", mask, argc);
		putenv(strdup(buff));
		return 0;
	}
	c = sscanf(string, "%lu %d%c", &m, &a, buff);
	if (c == 2 && m == mask) {
		/* 3rd */
		rmarg(environ, &string[-l - 1]);
		return 1 + (argc - a);
	}
	return -1;
}

#if !TRACEABLE

#define _LINUX_SOURCE_COMPAT
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#if !defined(PTRACE_ATTACH) && defined(PT_ATTACH)
#	define PTRACE_ATTACH	PT_ATTACH
#endif
void untraceable(char * argv0)
{
	char proc[80];
	int pid, mine;

	switch(pid = fork()) {
	case  0:
		pid = getppid();
		/* For problematic SunOS ptrace */
#if defined(__FreeBSD__)
		sprintf(proc, "/proc/%d/mem", (int)pid);
#else
		sprintf(proc, "/proc/%d/as",  (int)pid);
#endif
		close(0);
		mine = !open(proc, O_RDWR|O_EXCL);
		if (!mine && errno != EBUSY)
			mine = !ptrace(PTRACE_ATTACH, pid, 0, 0);
		if (mine) {
			kill(pid, SIGCONT);
		} else {
			perror(argv0);
			kill(pid, SIGKILL);
		}
		_exit(mine);
	case -1:
		break;
	default:
		if (pid == waitpid(pid, 0, 0))
			return;
	}
	perror(argv0);
	_exit(1);
}
#endif /* !TRACEABLE */

char * xsh(int argc, char ** argv)
{
	char * scrpt;
	int ret, i, j;
	char ** varg;
	char * me = argv[0];

	stte_0();
	 key(pswd, pswd_z);
	arc4(msg1, msg1_z);
	arc4(date, date_z);
	if (date[0] && (atoll(date)<time(NULL)))
		return msg1;
	arc4(shll, shll_z);
	arc4(inlo, inlo_z);
	arc4(xecc, xecc_z);
	arc4(lsto, lsto_z);
	arc4(tst1, tst1_z);
	 key(tst1, tst1_z);
	arc4(chk1, chk1_z);
	if ((chk1_z != tst1_z) || memcmp(tst1, chk1, tst1_z))
		return tst1;
	ret = chkenv(argc);
	arc4(msg2, msg2_z);
	if (ret < 0)
		return msg2;
	varg = (char **)calloc(argc + 10, sizeof(char *));
	if (!varg)
		return 0;
	if (ret) {
		arc4(rlax, rlax_z);
		if (!rlax[0] && key_with_file(shll))
			return shll;
		arc4(opts, opts_z);
		arc4(text, text_z);
		arc4(tst2, tst2_z);
		 key(tst2, tst2_z);
		arc4(chk2, chk2_z);
		if ((chk2_z != tst2_z) || memcmp(tst2, chk2, tst2_z))
			return tst2;
		/* Prepend hide_z spaces to script text to hide it. */
		scrpt = malloc(hide_z + text_z);
		if (!scrpt)
			return 0;
		memset(scrpt, (int) ' ', hide_z);
		memcpy(&scrpt[hide_z], text, text_z);
	} else {			/* Reexecute */
		if (*xecc) {
			scrpt = malloc(512);
			if (!scrpt)
				return 0;
			sprintf(scrpt, xecc, me);
		} else {
			scrpt = me;
		}
	}
	j = 0;
	varg[j++] = argv[0];		/* My own name at execution */
	if (ret && *opts)
		varg[j++] = opts;	/* Options on 1st line of code */
	if (*inlo)
		varg[j++] = inlo;	/* Option introducing inline code */
	varg[j++] = scrpt;		/* The script itself */
	if (*lsto)
		varg[j++] = lsto;	/* Option meaning last option */
	i = (ret > 1) ? ret : 0;	/* Args numbering correction */
	while (i < argc)
		varg[j++] = argv[i++];	/* Main run-time arguments */
	varg[j] = 0;			/* NULL terminated array */
#if DEBUGEXEC
	debugexec(shll, j, varg);
#endif
	execvp(shll, varg);
	return shll;
}

int main(int argc, char ** argv)
{
#if DEBUGEXEC
	debugexec("main", argc, argv);
#endif
#if !TRACEABLE
	untraceable(argv[0]);
#endif
	argv[1] = xsh(argc, argv);
	fprintf(stderr, "%s%s%s: %s\n", argv[0],
		errno ? ": " : "",
		errno ? strerror(errno) : "",
		argv[1] ? argv[1] : "<null>"
	);
	return 1;
}
